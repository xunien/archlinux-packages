#!/bin/bash

set -eo pipefail

_log() {
    printf "==> %s\n" "$@"
}

_log_err() {
    echo "$1"
    exit 1
}

installarg=""
if [ -n "$1" ] && [ "$1" == "--install" ] || [ "$1" == "-i" ]; then
    installarg="--install"
fi

git ls-tree -d --name-only HEAD | while read -r pkg
do
    pushd $pkg
    _log "$pkg"
    makepkg --noconfirm --cleanbuild --clean --syncdeps --force $installarg
    if [ $? -ne 0 ]; then
        _log_err "$pkg failed"
    fi
    popd
done
