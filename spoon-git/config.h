/* delay between each update in seconds */
int delay = 5;

struct ent ents[] = {
    { .fmt = "[%s] ",   .read = loadread,       .arg = NULL },
    { .fmt = "[%s] ",   .read = cpuread,        .arg = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq" },
    { .fmt = "[%s° ",   .read = tempread,       .arg = "/sys/class/hwmon/hwmon4/temp1_input" },
    { .fmt = "%s° ",    .read = tempread,       .arg = "/sys/class/hwmon/hwmon4/temp2_input" },
    { .fmt = "%s°] ",   .read = tempread,       .arg = "/sys/class/hwmon/hwmon4/temp3_input" },
    { .fmt = "%s ",     .read = battread,       .arg = &(struct battarg){ .cap = "/sys/class/power_supply/BAT0/capacity", .ac = "/sys/class/power_supply/AC/online" } },
    { .fmt = "%s ",     .read = wifiread,       .arg = "wlp3s0" },
    { .fmt = "[%s] ",   .read = xkblayoutread,  .arg = NULL },
    { .fmt = "%s",      .read = dateread,       .arg = &(struct datearg){ .fmt = "%a %d %b %Y %H:%M %Z", .tz = NULL } },
};
